import {
  ActionRowBuilder,
  Client,
  CommandInteraction,
  ModalActionRowComponentBuilder,
  ModalBuilder,
  TextInputBuilder,
  Snowflake,
  TextInputStyle,
} from "discord.js";
import { Command } from "src/models";

export const AddAnswers: Command = {
  name: "ajouter-reponses",
  description: "Propose de nouvelles réponses pour le Seau du Rat",
  type: 1,
  run: async (client: Client, interaction: CommandInteraction) => {
    const modal = new ModalBuilder()
      .setCustomId("answers-modal")
      .setTitle("Ajouter des réponses au Seau du Rat");

    const answersInput = new TextInputBuilder()
      .setCustomId("answersInput")
      .setLabel("Insérez des réponses pour le Seau du Rat")
      .setPlaceholder("Merci de se référer au formatage fourni sur le fil")
      .setStyle(TextInputStyle.Paragraph);
    const actionRow =
      new ActionRowBuilder<ModalActionRowComponentBuilder>().addComponents(
        answersInput
      );
    modal.addComponents(actionRow);
    await interaction.showModal(modal);
  },
};
