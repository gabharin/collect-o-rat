import { Command } from "src/models";
import { AddAnswers } from "./AddAnswers";
import { AddPrompts } from "./AddPrompts";

export const Commands: Command[] = [AddAnswers, AddPrompts];
