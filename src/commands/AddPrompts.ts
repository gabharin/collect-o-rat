import {
  ActionRowBuilder,
  Client,
  CommandInteraction,
  ModalActionRowComponentBuilder,
  ModalBuilder,
  TextInputBuilder,
  TextInputStyle,
} from "discord.js";
import { Command } from "src/models";

export const AddPrompts: Command = {
  name: "ajouter-questions",
  description: "Propose de nouvelles questions pour le Seau du Rat",
  type: 1,
  run: async (client: Client, interaction: CommandInteraction) => {
    const modal = new ModalBuilder()
      .setCustomId("prompts-modal")
      .setTitle("Ajouter des questions au Seau du Rat");

    const promptsInput = new TextInputBuilder()
      .setCustomId("promptsInput")
      .setLabel("Insérez des question pour le Seau du Rat")
      .setPlaceholder("Merci de se référer au formatage fourni sur le fil")
      .setStyle(TextInputStyle.Paragraph);
    const actionRow =
      new ActionRowBuilder<ModalActionRowComponentBuilder>().addComponents(
        promptsInput
      );
    modal.addComponents(actionRow);
    await interaction.showModal(modal);
  },
};
