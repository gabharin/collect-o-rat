import { Client } from "discord.js";
import interactionCreate from "./listeners/interaction-create";
import ready from "./listeners/ready";
import mongoose from "mongoose";

const answerSchema = new mongoose.Schema({
  text: String,
  validated: { type: Boolean, default: false },
});
export const Answer = mongoose.model("Answer", answerSchema);
const promptSchema = new mongoose.Schema({
  text: String,
  validated: { type: Boolean, default: false },
});
export const Prompt = mongoose.model("Prompt", promptSchema);

async function main() {
  console.log("[COLLECT-O-RAT] Starting...");

  const client = new Client({
    intents: [],
  });

  client.login(process.env.DISCORD_TOKEN);

  await mongoose.connect(
    `mongodb://${process.env.MONGODB_HOST}:${process.env.MONGODB_PORT}/${process.env.MONGODB_DATABASE}`
  );
  ready(client);
  interactionCreate(client);
}

main();
