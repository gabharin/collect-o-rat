import { CommandInteraction, Client, Interaction, Events } from "discord.js";
import { Commands } from "../commands/Commands";
import { Answer, Prompt } from "../Bot";

export default (client: Client): void => {
  client.on(Events.InteractionCreate, async (interaction: Interaction) => {
    if (interaction.isCommand() || interaction.isContextMenuCommand()) {
      await handleSlashCommand(client, interaction);
    }
    if (
      interaction.isModalSubmit() &&
      interaction.customId === "answers-modal"
    ) {
      if (interaction.fields.fields.get("answersInput")) {
        const answers = interaction.fields.fields
          .get("answersInput")
          ?.value.split("\n");
        if (answers) {
          const setAnswers = new Set(answers);
          for (let text of setAnswers) {
            text = text.trim();
            if (text !== "") {
              const dbAnswer = new Answer({ text });
              dbAnswer.save();
            }
          }
          await interaction.reply({
            content: "Merci pour votre ajout !",
            ephemeral: true,
          });
        }
      }
    }
    if (
      interaction.isModalSubmit() &&
      interaction.customId === "prompts-modal"
    ) {
      if (interaction.fields.fields.get("promptsInput")) {
        const prompts = interaction.fields.fields
          .get("promptsInput")
          ?.value.split("\n");
        if (prompts) {
          const setPrompts = new Set(prompts);
          for (let text of setPrompts) {
            text = text.trim();
            text = text.replace("_", "____");
            if (text !== "") {
              const dbPrompt = new Prompt({ text });
              dbPrompt.save();
            }
          }
          await interaction.reply({
            content: "Merci pour votre ajout (no homo) !",
            ephemeral: true,
          });
        }
      }
    }
  });
};

const handleSlashCommand = async (
  client: Client,
  interaction: CommandInteraction
): Promise<void> => {
  const slashCommand = Commands.find((c) => c.name === interaction.commandName);
  if (!slashCommand) {
    interaction.followUp({ content: "Could not find command..." });
    return;
  }
  slashCommand.run(client, interaction);
};
